# TileVania
A fast-paced classic Side-Scrolling Platformer developed with C# and Unity for learning purposes:

* Game design
* Working with sprite sheets
* Unity rule tiles
* Animation states & transitions
* Managing animation state in code
* Perspective Vs Orthographic cameras
* Cinemachine Follow Camera
* State-Driven Cameras

## Game features:
* Follow camera
* Enemies and hazards
* Score, lives and coins system
* Coins pickups


## How To Build / Compile
This is a Unity project, therefore it requires Unity3D to build. Clone or download this repo, and navigate to `Assets > Scenes` then open any `.unity` file.
